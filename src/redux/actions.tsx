export const SET_USERS = 'SET_USERS';
export const SET_USER_TODOS = 'SET_USER_TODOS';
export const SET_TASK_DONE = 'SET_TASK_DONE';

export const setUsers = users => dispatch => {
  dispatch({
    type: SET_USERS,
    payload: users,
  });
};

export const setUserToDos = todos => dispatch => {
  dispatch({
    type: SET_USER_TODOS,
    payload: todos,
  });
};

export const setTaskDone = task => dispatch => {
  dispatch({
    type: SET_TASK_DONE,
    payload: task,
  });
};
