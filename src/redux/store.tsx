import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import appsReducers from './reducers';

const rootReducer = combineReducers({appsReducers});

export const Store = createStore(rootReducer, applyMiddleware(thunk));
