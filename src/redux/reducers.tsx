import {SET_USERS, SET_USER_TODOS, SET_TASK_DONE} from './actions';

const initialState = {
  users: [],
  todos: [],
  taskDone: false,
};

function appsReducers(state = initialState, action) {
  switch (action.type) {
    case SET_USERS:
      return {...state, users: action.payload};

    case SET_USER_TODOS:
      return {...state, todos: action.payload};

    case SET_TASK_DONE:
      return {...state, taskDone: action.payload};

    default:
      return state;
  }
}

export default appsReducers;
