import * as React from 'react';
import {
  SafeAreaView,
  FlatList,
  StyleSheet,
  StatusBar,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

// API
import API from '../api/users_todos';

// HTTP Request to get the User's To-Do's by User-ID.
const initiateGetUsers_TODO = async (userID, onSucess, onFail) => {
  try {
    const response = await API.get(`/users/${userID}/todos`, {});
    onSucess(response.data);
  } catch (error) {
    console.log(error);
    onFail();
  }
};

function User_ToDo_Screen({navigation, route}) {
  // Users List Hook
  const [userToDos, setUserToDos] = React.useState([]);
  // Error & Activity-Indicator Hooks
  const [errorMessage, setErrorMessage] = React.useState('');
  const [isWaitingState, setIsWaitingState] = React.useState(true);

  // Shows spinner while HTTP request is done.
  const showIndicatorIfNeeded = () => {
    return isWaitingState ? (
      <ActivityIndicator
        style={{
          height: 100,
        }}
      />
    ) : null;
  };

  // Show Error message for network issues.
  const showErrorMessage = () => {
    return errorMessage != '' ? (
      <Text style={{color: 'red'}}>{errorMessage}</Text>
    ) : null;
  };

  // Runs once: Changing the Screen Title, Making the HTTP request for To-Do's.
  React.useEffect(() => {
    // Changing Screen Title
    const ScreenTitle = `${route.params.name}'s To Dos:`;
    navigation.setOptions({title: ScreenTitle});
    // IF To-Do list isn't empty, exit.
    if (typeof userToDos !== 'undefined' && userToDos.length !== 0) return;
    setIsWaitingState(true);
    initiateGetUsers_TODO(
      route.params.id,
      data => {
        setUserToDos(data);
        setIsWaitingState(false);
      },
      () => {
        console.log('ERROR');
        setIsWaitingState(false);
        setErrorMessage(`Error getting ${route.params.name}'s To Dos!`);
      },
    );
  });

  // To-Do List Item
  const ItemUser = ({title, completed, id}) => (
    <View style={styles.item}>
      <View style={{justifyContent: 'center'}}>
        <Text>{title}</Text>
      </View>
      <CheckBox
        disabled={false}
        onValueChange={newValue => {
          var newToDos = userToDos.filter(function (obj) {
            return obj.id !== id;
          });
          setTimeout(function () {
            setUserToDos(newToDos);
          }, 1000);
        }}
      />
    </View>
  );

  // List Item rendering
  const renderItemUser = ({item}) => (
    <ItemUser title={item.title} completed={item.completed} id={item.id} />
  );

  // Visual JSX
  return (
    <SafeAreaView style={styles.container}>
      {showIndicatorIfNeeded()}
      {showErrorMessage()}
      <FlatList
        style={{backgroundColor: '#91bbff'}}
        data={userToDos}
        renderItem={renderItemUser}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
}

// StyleSheet
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 8,
    backgroundColor: '#ffffff',
    padding: 5,
    margin: 5,
  },
});

export default User_ToDo_Screen;
