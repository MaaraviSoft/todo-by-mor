import React from 'react';
import {
  SafeAreaView,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

// Redux
import {useSelector, useDispatch} from 'react-redux';
import {setUsers} from '../redux/actions';

// API
import API from '../api/users_todos';

// Users List Item (one user)
const ItemUser = ({id, name, email, navigation}) => (
  <TouchableOpacity
    style={styles.item}
    onPress={() => {
      const UserInfo = {id: id, name: name, email: email};
      navigation.navigate('UserToDos', UserInfo);
    }}>
    <Text style={styles.id}>{id}</Text>
    <Text style={styles.name}>{name}</Text>
    <Text style={styles.email}>{email}</Text>
  </TouchableOpacity>
);

const UserScreen = ({navigation}) => {
  // Error & Activity-Indicator Hooks
  const [errorMessage, setErrorMessage] = React.useState('');
  const [isWaitingState, setIsWaitingState] = React.useState(true);
  // Users in Redux
  const {users} = useSelector(state => state.appsReducers);
  const dispatch = useDispatch();
  // HTTP Request to get the User.
  const initiateGetUsers = async (onSucess, onFail) => {
    try {
      const response = await API.get('/users', {});
      onSucess(response.data);
    } catch (error) {
      console.log(error);
      onFail();
    }
  };

  // Runs once: Getting the Users List IF not already gotten.
  React.useEffect(() => {
    if (typeof users !== 'undefined' && users.length !== 0) return;
    initiateGetUsers(
      data => {
        dispatch(setUsers(data));
        setIsWaitingState(false);
      },
      () => {
        setIsWaitingState(false);
        setErrorMessage('Something went wrong :(');
      },
    );
  });

  // Shows spinner while HTTP request is done.
  const showIndicatorIfNeeded = () => {
    return isWaitingState ? (
      <ActivityIndicator
        style={{
          height: 100,
        }}
      />
    ) : null;
  };

  // Show Error message for network issues.
  const showErrorMessage = () => {
    return errorMessage != '' ? (
      <Text style={{color: 'red'}}>{errorMessage}</Text>
    ) : null;
  };

  // Users List Renderer
  const renderItemUser = ({item}) => (
    <ItemUser
      id={item.id}
      name={item.name}
      email={item.email}
      navigation={navigation}
    />
  );

  // Visual JSX
  return (
    <SafeAreaView style={styles.container}>
      {showIndicatorIfNeeded()}
      {showErrorMessage()}
      <FlatList
        data={users}
        renderItem={renderItemUser}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
};
// StyleSheet
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#91bbff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },

  id: {
    fontSize: 11,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 32,
  },
  email: {
    fontSize: 22,
  },
});

export default UserScreen;
