// Navigation
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// React
import * as React from 'react';
// Redux
import {Provider} from 'react-redux';
import {Store} from './src/redux/store';

import UserScreen from './src/screens/users';
import UserToDos from './src/screens/user_todos';

const Stack = createStackNavigator();

export default function App() {
  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Users">
          <Stack.Screen name="Users" component={UserScreen} />
          <Stack.Screen name="UserToDos" component={UserToDos} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
